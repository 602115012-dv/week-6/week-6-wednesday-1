import React from 'react';
import Display from './display';
import Keypad from './keypad';

class Calculator extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      expression: '',
    }
  }

  clearDisplay = () => {
    this.setState({
      expression: '',
    });
  }

  expressionCallback = (respond) => {
    console.log(respond);
    this.setState({
      expression: this.state.expression.concat(respond)
    });
  }

  resultCallback = (respond) => {
    this.setState({
      expression: respond.toString()
    });
  }

  render() {
    return (
      <div className="container" style={{marginTop: "20px"}}>
        <div className="row justify-content-center">
          <div className="col-2">
            <div className="row  justify-content-center">
              <Display expression={this.state.expression}/>
              <Keypad expressionCallback={this.expressionCallback} expression={this.state.expression} 
              resultCallback={this.resultCallback} clearDisplay={this.clearDisplay}/>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Calculator