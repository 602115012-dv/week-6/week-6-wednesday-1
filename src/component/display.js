import React from 'react';

class Display extends React.Component {
  render() {
    return (
      <div className="col-12">
        <div style={{border: "1px solid #a6a6a6", borderRadius: "4px"}}>
          <input type="text" id="display" value={
            this.props.expression
            } style={{width: "100%"}} disabled />
        </div>
      </div>
    );
  }
}

export default Display