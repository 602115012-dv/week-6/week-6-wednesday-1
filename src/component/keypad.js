import React from 'react';

class Keypad extends React.Component {
  render() {
    return(
      <div className="col-12">
        <table>
          <tbody>
            <tr>
              <td colSpan="4" style={{width: "100%"}}>
                <button className="btn btn-danger"  onClick={this.clearDisplay} style={{width: "100%"}}>
                  Clear
                </button>
              </td>
            </tr>
            <tr>
              <td><button className="btn btn-secondary" onClick={() => this.addOperand(7)}>7</button></td>
              <td><button className="btn btn-secondary" onClick={() => this.addOperand(8)}>8</button></td>
              <td><button className="btn btn-secondary" onClick={() => this.addOperand(9)}>9</button></td>
              <td><button className="btn btn-primary" onClick={() => this.addOperator(' / ')}>/</button></td>
            </tr>
            <tr>
              <td><button className="btn btn-secondary" onClick={() => this.addOperand(4)}>4</button></td>
              <td><button className="btn btn-secondary" onClick={() => this.addOperand(5)}>5</button></td>
              <td><button className="btn btn-secondary" onClick={() => this.addOperand(6)}>6</button></td>
              <td><button className="btn btn-primary" onClick={() => this.addOperator(' * ')}>*</button></td>
            </tr>
            <tr>
              <td><button className="btn btn-secondary" onClick={() => this.addOperand(1)}>1</button></td>
              <td><button className="btn btn-secondary" onClick={() => this.addOperand(2)}>2</button></td>
              <td><button className="btn btn-secondary" onClick={() => this.addOperand(3)}>3</button></td>
              <td><button className="btn btn-primary" onClick={() => this.addOperator(' + ')}>+</button></td>
            </tr>
            <tr>
              <td><button className="btn btn-secondary" onClick={() => this.addOperand(0)}>0</button></td>
              <td><button className="btn btn-secondary" onClick={() => this.addDot('.')}>.</button></td>
              <td><button className="btn btn-success" onClick={() => this.calculate()}>=</button></td>
              <td><button className="btn btn-primary" onClick={() => this.addOperator(' - ')}>-</button></td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }

  addDot(dot) {
    if (this.props.expression.length != 0){
      this.props.expressionCallback(dot);
    }
  }

  addOperand(operand) {
    this.props.expressionCallback(operand);
  }

  addOperator(operator) {
    let lastChar = this.props.expression[this.props.expression.length - 2];
    if (lastChar === '+' || lastChar === '-' || lastChar === '/' || lastChar === '*' 
    || this.props.expression.length == 0) {
      return;
    }
    this.props.expressionCallback(operator);
  }

  calculate() {
    this.props.resultCallback(eval(this.props.expression));
  }

  clearDisplay = () => {
    this.props.clearDisplay();
  }
}

export default Keypad